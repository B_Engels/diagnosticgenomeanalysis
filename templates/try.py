#!/usr/bin/env python3
"""
Your program should have the following functionality:
Accept the following input parameters using the argparse Python library:
Database name
Database username
Database password
ANNOVAR annotation file
(optional) database schema file (.sql file for (re-)creating the database)
Test the given database name and credentials (i.e. perform a simple SELECT 1 query)
The program thus needs to connect to the database (on the mysql.bin MySQL server)
Test if the given ANNOVAR file exists and if it contains tab-separated data
(i.e., the length of the tab-splitted
header should be > 1) If given and the previous test(s) pass, execute the
.sql file (this will DROP all existing
table(s)) Parse the ANNOVAR file (use the code from deliverable 6)
Either build up a proper object containing the data and logic for parsing (see 'bonus' below) or
Process the parsed data on-the-fly;
Once you have the chromosome information; insert into the Chromosome table,
Once you have the gene information (parsed by your deliverable 7 code), insert into the Gene table,
Once you have the variant information, insert into the Variant table.
Once all data has been parsed and inserted into the database, a small report should
be printed that states the number
of rows in each table. This should be added to your OneNote
journal file. And the database and/ or file
connections should be closed
"""
# METADATA
__author__ = 'Bart Engels'
__status__ = 'Template'
__version__ = '2019.d9.v1'

#imports
import sys
import argparse as asg
import mysql.connector
from mysql.connector import errorcode

class AnoMysqlParser:
    """
    this class parser the anovar and put it in a database
    """
    def __init__(self, d_name, u_name, p_word, ano_file):
        """
        """
        self.my_db = mysql.connector.connect(
                host='mariadb.bin',
                user=u_name,
                passwd=p_word,
                database=d_name
            )
        self.ano = ano_file

        '''self.d_name = d_name
        self.u_name = u_name
        self.p_word = p_word
        self.a_file = ano_file'''

    def conect_mysql(self):
        """

        :return:
        """
        ''' my_db = mysql.connector.connect(
                       host='mariadb.bin',
                       user=self.u_name,
                       passwd=self.p_word,
                       database=self.d_name)'''
        try:
            self.my_db


        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
                print('Something is wrong with your user name or password')
            elif err.errno == errorcode.ER_BAD_DB_ERROR:
                print('Database does not exist')
            elif err.errno == errorcode.ER_ACCESS_DENIED_NO_PASSWORD_ERROR:
                print('No password!!')
            else:
                print(err)


        else:
            my_cur = self.my_db.cursor()
            my_cur.execute("SHOW TABLES")
            tables = my_cur.fetchall()
            return tables


    def check_anovar(self):
        """

        :return:
        """



        return 0

    def drop_tables(self):
        """

        :return:
        """
        return 0

    def process_annovar_line(self, variant, header):
        """

        :param variant:
        :param header:
        :return:
        """
        return 0

    def parse_annovar(self):
        """
        def __str__(self):
        return str(json.dumps(self.parse_annovar(), indent=4))
        """
        return 0





def main(args):
    """

    :param args:
    :return:
    """
    parser = asg.ArgumentParser()

    parser.add_argument('d_name', help='Give a Database name')
    parser.add_argument('username', help='Give your username')
    parser.add_argument('password', help='Give te password that belongs to your username')
    parser.add_argument('annovar_file', help='Annovar annotation file')

    args = parser.parse_args()
    d_name = args.d_name
    u_name = args.username
    p_word = args.password
    ano_file = args.annovar_file

    a_mysql = AnoMysqlParser(d_name, u_name, p_word, ano_file)
    print(a_mysql.conect_mysql())
    return 0

if __name__ == "__main__":
    sys.exit(main(sys.argv))
