Author Bart Engels
Date   19-12-2019
Version 2019.d2.v2

Name
Thema 5 deliverable9

Description
Dit programma maakt een database van een tabular file (bijgevoegd) het haald informatie uit dit bestand en stopt het in
de juiste collum van de database.


Requirements
need to install te next coding language to run this program:
* Python 3.7
 used modules:
    * msyql
    * mysql.connector
    * argparse

Usage
step1:
Login in de  Database en laat het bestand deliverable8.sql file in.
source deliverable8.sql

step2:
open het bestand .my.cnf en voer hier jouw database gegevens in.
Sla dit op en zet het in de zelfde map als deliverable9,py en data.tabular

step3:
open de terminal en ga naar de map waar de bestanden van het zip bestand in zitten unzip deze. daarna :
vul het .my.cnf bestand in. (bijgevoegd)
python3 deliverable9.py .my.cnf data.tabular

step4:
open de database en u kunt de tabelen zien:
select * from chromosomes;
select * from genes;
select * from variants;


Support
Met vragen kunt u terecht bij: b.engels@st.hanze.nl
of
(06-21715490)
