/* Bestand voor het aanmaken van de tabellen van Deliverable 8.
*/

drop table if exists variants, genes, chromosomes;


create table chromosomes
(   chrom_id            int auto_increment  not null,
    CHROM               varchar(50) unique,
    primary key (chrom_id)

);

/* Deze tabel 'refereert' naar chromosomes
*/
create table genes
(   gene_id             int auto_increment  not null,
    chrom_id            int                         not null,
    RefSeq_Gene         varchar(255)    unique,
    primary key (gene_id),
    foreign key (chrom_id)
        references chromosomes(chrom_id)
);

/* Deze tabel 'refereert' naar genes
*/
create table variants
(   var_id              int auto_increment  not null,
    POS                 int,
    gene_id             int                 not null,
    reference           char(1),
    observed            char(1),
    dbsnp138            varchar(50),
    1000g2015aug_EUR    float,
    LJB2_SIFT           float,
    LJB2_Polyphen2_HDIV varchar(50),
    LJB2_Polyphen2_HVAR varchar(50),
    CLINVAR             varchar(150),
    primary key (var_id),
    foreign key (gene_id)
        references genes(gene_id)
 );
