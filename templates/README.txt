Thema 05 - Genome Project


Authors: Bart Engels & Marije Hibma
Date: 01/11/2019
Version: 1.0

--------------------------------------------------------------

Files:

Deliverable1.py		Parsing BED data.
Deliverable2.py		Parsing and filtering pileup data.Pileup data falling within the exons in the 				'bed_dict' is stored in a dictionary. This dictionary should contain a coverage 			number for each position within the ~50 genes from the cardiopanel (BED file).
Deliverable3.py		Reading in data and writing output data. Running this script
			will print the first three lines of the input BED and pileup files and
			write the coverage statistics to the file 'd3_output.csv'.
Deliverable4.py		Contains the first three deliverables merged into one script.
Deliverable5.py		Parsing and filtering VCF data given a certain variant
			allele frequency value.
Deliverable6.py		A program that reads in an ANNOVAR output file
Deliverable7.py		Contains a function that gets the value from this annotation column and returns the 				actual gene name(s) by 'extracting' it using a regular expression and filtering it 				using standard Python code.
Deliverable8.sql	Create an SQL database schema that consists of three tables to store information from 				the (i) Chromosomes, (ii) Genes and (iii) Variants. SQL file containing the CREATE 				TABLE statements
Deliverable9.py		Python code that directly connects to MySQL and executes the CREATE and INSERT 				statements. The supporting library to use is mysql.connector.
