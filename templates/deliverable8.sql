
DROP TABLE IF EXISTS variants;
DROP TABLE IF EXISTS genes;
DROP TABLE IF EXISTS chromosomes;

create table chromosomes
(   chr_id            int auto_increment  not null,
    chromosome        varchar(50) unique,
    primary key (chr_id)

);

create table genes
(   gene_id             int auto_increment  not null,
    chr_id              int  not null,
    RefSeq_Gene         varchar(255),
    primary key (gene_id),
    foreign key (chr_id)
        references chromosomes(chr_id)
);


create table variants
(   f_id                int auto_increment not null,
    gene_id             int not null,
    POS                 int,
    dbsnp138            varchar(50)  ,
    RefSeq_Func varchar(50),
    1000g2015aug_EUR    varchar(50)  ,
    LJB2_SIFT           varchar(50)  ,
    LJB2_PolyPhen2_HDIV varchar(50)  ,
    LJB2_PolyPhen2_HVAR varchar(50),
    CLINVAR             varchar(255)  ,
    reference           char(1),
    observed            char(1),
    primary key (f_id),
    foreign key (gene_id)
        references genes(gene_id)
);
