#!/usr/bin/env python3

"""
BFV2 Theme 05 - Genomics - Sequencing Project

Template for a program performing the following steps:
----------------------------------------------------------
* Load the BED file containing the information (names, chromosome and
  coordinates of the exons) of all cardiopanel genes
* Load the pileup file containing the mapping data
* For each exon found in the BED file:
    * read the start- and end-coordinate
    * find all entries in the pileup file for this chromosome and within
      these coordinates
    * for each pileup-entry:
        * store the coverage (data from column 4)
* Given the found coverage for each position in all exons:
    * Calculate the average coverage per gene
    * Count the number of positions with a coverage < 30
* Write a report on all findings (output to Excel-like file)

Deliverable 4
-------------
This template contains a number of placeholders where you are asked to place
your own code made in previous deliverables, following the instructions
preceded with double '##' symbols.

The 'main()' functions glues all your functions into a single coherent
program that performs all required steps.

* Note: Test the program on the example data first.
* Note: by default the 'data/example.bed' and 'data/example.pileup' files are
        used as input, but you can supply your own files on the
        commandline.

    usage:
        python3 deliverable4.py [bed-file.bed] [pileup-file.pileup]
"""

# METADATA VARIABLES
__author__ = "Bart Engels & Marije Hibma"
__status__ = "program"
__version__ = "2018.d4.v1"

# IMPORT
import sys
import csv
import statistics

# FUNCTIONS
def read_data(filename):
    """ This function reads in data and returns a list containing one
        line per element. """
    with open(filename) as file:
        return [line for line in file]

def parse_bed_data(bed_data):
    """ Function that parses BED data and stores its contents
        in a dictionary
    """

    ## Iterate over all lines in the 'bed_data' list and fill the
    for it in bed_data:
        s_line = it.split()
        chromo = s_line[0]
        start = int(s_line[1])
        stop = int(s_line[2])
        g_naam = s_line[3]
        if chromo not in bed_dict.keys():
            ## `bed_dict` dictionary with the `chromosome` as key. The other fields
            bed_dict[chromo] = []
            ## are added as a tuple using the correct types.
            bed_dict[chromo].append((start, stop, g_naam))
        else:
            bed_dict[chromo].append((start, stop, g_naam))
    ## Return the bed_dict one all lines are done
    return bed_dict

def parse_pileup_data(pileup_data, bed_dict):
    """ Function that parses pileup data and collects the per-base coverage
    of all exons contained in the BED data.

    Iterate over all pileup lines and for each line:
        - check if the position falls within an exon (from `bed_dict`)
            - if so; add the coverage to the `coverage_dict` for the correct gene
    """
    coverage_dict = {}
    # Iterate over all the lines contained in the pileup_data
    for line in pileup_data:
        # Extract the 'chromosome' field and remove the 'chr' text
        line = line.split("\t")
        chromosome = line[0].lstrip('chr')
        # Other fields
        info = line[1:]

        if chromosome in bed_dict.keys():
            # Extract the coordinate from the pileup and compare to all exons for that chromosome in the `bed_dict`
            for i in range(len(bed_dict[chromosome])):
                # coordinate left <= coordinate pileup <= coordinate right
                if bed_dict[chromosome][i][0] <= int(info[0]) < bed_dict[chromosome][i][1]:
                    # Check if the chromosome is contained in the bed_dict
                    if bed_dict[chromosome][i][2] not in coverage_dict.keys():
                        # Add coverage
                        coverage_dict[bed_dict[chromosome][i][2]] = [int(info[2])]
                    else:
                        # Add coverage
                        coverage_dict[bed_dict[chromosome][i][2]].append(int(info[2]))
    return coverage_dict

def calculate_mapping_coverage(coverage_dict):
    """ Function to calculate all coverage statistics on a per-gene basis
        and store this in a list.
        Note: this function is taken from deliverable 5 and slightly modified
    """

    ## Create an empty list that will hold all data to save
    statistics_data = []

    ## Iterate over all the genes in the coverage_dict getting the gene name
    for item in coverage_dict:
        ## And list with coverage data for that gene

        ## Gene name
        genename = item
        ## Total positions (gene length covered)
        cover_len = len(coverage_dict[genename])
        ## Average Coverage (use round with one position)
        average = "{:.1f}".format(statistics.mean(coverage_dict[genename]))
        ## Number of low-coverage positions (coverage value < 30)
        counter = 0
        low_pos = (coverage_dict[genename])
        for l_pos in low_pos:
            if l_pos < 30:
                counter += 1
        ## Put the elements in a single tuple and append to the
        ## statistics list.
        tup = (genename, cover_len, average, counter)
        statistics_data.append(tup)

    # Returns the list of tuples holding the data for a statistics_data:
    for pard in statistics_data:
        stats = "\t".join([str(i) for i in pard])
        print(stats)

    return statistics_data

def save_coverage_statistics(coverage_file, coverage_statistics):
    """ Writes coverage data to a tabular file using Python's
        csv library: https://docs.python.org/3/library/csv.html#csv.writer
    """
    with open(coverage_file, 'w', newline="") as csvfile:
        csv_out = csv.writer(csvfile, delimiter='\t')

        for item in coverage_statistics:
            csv_out.writerow(item)
        return csvfile


######
# Do not change anything below this line
######

# MAIN
def main(args):
    """ Main function connecting all functions
        Note: the 'is None' checks that are done are only
        necessary for this program to run without error if
        not all functions are completed.
    """

    ### INPUT ###
    # Try to read input en output filenames from the commandline. Use defaults if
    # they are missing and warn if the extensions are 'wrong'.
    if len(args) > 1:
        bed_file = args[1]
        if not bed_file.lower().endswith('.bed'):
            print('Warning: given BED file does not have a ".bed" extension.')
        pileup_file = args[2]
        if not pileup_file.lower().endswith('.pileup'):
            print('Warning: given pileup file does not have a ".pileup" extension.')
        output_file = args[3]
    else:
        bed_file = 'data/example.bed'
        pileup_file = 'data/example.pileup'
        output_file = 'd4_output.csv'

    # STEP 1: Read BED data
    print('Reading BED data from', bed_file)
    bed_data = read_data(bed_file)
    if bed_data is None:
        print('No BED-data read...')
    else:
        print('\t> A total of', len(bed_data), 'lines have been read.\n')

    # STEP 2: Read Pileup data
    print('Reading pileup data from', pileup_file)
    pileup_data = read_data(pileup_file)
    if pileup_data is None:
        print('No Pileup-data read...')
    else:
        print('\t> A total of', len(pileup_data), 'lines have been read.\n')

    # STEP 3: Parsing BED data
    print('Parsing BED data...')
    bed_dict = parse_bed_data(bed_data)
    if bed_dict is None:
        print('BED-data not parsed!')
    else:
        print('\t> A total of', len(bed_dict.keys()), 'chromosomes have been stored.\n')

    # STEP 4: Parsing and filtering pileup data
    print('Parsing and filtering pileup-data...')
    coverage_dict = parse_pileup_data(pileup_data, bed_dict)
    if coverage_dict is None:
        print('Pileup data not parsed!')
    else:
        print('\t> Coverage of', len(coverage_dict.keys()), 'genes have been stored.\n')

    # STEP 5: Store calculated data
    print('Calculating coverage statistics...')
    coverage_statistics = calculate_mapping_coverage(coverage_dict)
    if coverage_statistics is None:
        print('No coverage statistics calculated!')
    else:
        print('\t> Statistics for', len(coverage_statistics), 'genes have been calculated.\n')

    # STEP 6: Write output data
    print('Writing the coverage statistics to', output_file)
    if coverage_statistics is None:
        print('Nothing to write, quitting...')
    else:
        save_coverage_statistics(output_file, coverage_statistics)
        from pathlib import Path
        csv_file_check = Path(output_file)
        if csv_file_check.is_file():
            print('\t> CSV file created, program finished.')
        else:
            print('\tCSV file', output_file, 'does not exist!')

    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv))
