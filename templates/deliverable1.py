#!/usr/bin/env python3

"""
BFV2 Theme 05 - Genomics - Sequencing Project

Simple template for parsing BED data.

Deliverable 1
-------------
Make changes to the 'parse_bed_data' function, following the instructions
preceded with double '##' symbols.

    usage:
        python3 deliverable1.py
"""

# METADATA VARIABLES
__author__ = "Bart Engels & Marije Hibma"
__status__ = "program"
__version__ = "2018.d1.v1"

# IMPORT
import sys

# FUNCTIONS
def parse_bed_data(bed_data):
    """ Function that parses BED data and stores its contents
        in a dictionary
    """

    ## Create empty dictionary to hold the data
    bed_dict = {}

    #file = input("bedfile: ")
    #r_file = open(file)
    ## Iterate over all lines in the 'bed_data' list and fill the
    for line in bed_data:
        s_line = line.split()
        chromo = s_line[0]
        start = int(s_line[1])
        stop = int(s_line[2])
        g_naam = s_line[3]
        if chromo not in bed_dict.keys():
            ## `bed_dict` dictionary with the `chromosome` as key. The other fields
            bed_dict[chromo] = []
            ## are added as a tuple using the correct types.
            bed_dict[chromo].append((start, stop, g_naam))
        else:
            bed_dict[chromo].append((start, stop, g_naam))

    ## Return the bed_dict one all lines are done
    return bed_dict

######
# Do not change anything below this line
######

# MAIN
def main(args):
    """ Main function that tests for correct parsing of BED data """
    ### INPUT ###
    bed_data = [
        "1	237729847	237730095	RYR2",
        "1	237732425	237732639	RYR2",
        "1	237753073	237753321	RYR2",
        "18	28651551	28651827	DSC2",
        "18	28654629	28654893	DSC2",
        "18	28659793	28659975	DSC2",
        "X	153648351	153648623	TAZ",
        "X	153648977	153649094	TAZ",
        "X	153649222	153649363	TAZ"
    ]

    ### OUTPUT ###
    expected_bed_dict = {
        '1':  [(237729847, 237730095, 'RYR2'),
               (237732425, 237732639, 'RYR2'),
               (237753073, 237753321, 'RYR2')],
        '18': [(28651551, 28651827, 'DSC2'),
               (28654629, 28654893, 'DSC2'),
               (28659793, 28659975, 'DSC2')],
        'X':  [(153648351, 153648623, 'TAZ'),
               (153648977, 153649094, 'TAZ'),
               (153649222, 153649363, 'TAZ')]}

    # Call the parse-function
    bed_dict = parse_bed_data(bed_data)
    _assert_output_vs_expected(bed_dict, expected_bed_dict)

def _assert_output_vs_expected(output, expected):
    """ Compares given output with expected output.
    Do not modify. """
    import unittest
    if isinstance(output, dict):
        testcase = unittest.TestCase('__init__')
        try:
            testcase.assertDictEqual(expected, output,
                                     msg="\n\nUnfortunately, the output is *not* correct..")
        except AssertionError as error:
            print(error)
            return 0
        print("\nWell done! Output is correct!")
        return 1
    print("\n\nUnfortunately, the output is *not* a dictionary!")
    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv))
