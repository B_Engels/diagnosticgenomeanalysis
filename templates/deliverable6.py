#!/usr/bin/env python3

"""
Notes:

Pre-define the columns to extract, i.e. columns = [10, 11, 15:18, 20:24]
Combine this with the parsed header string
("chromosome\tbegin\tend\treference\tobserved\tCHROM\tPOS...")
Output of this deliverable should be a printed summary of the data for each variant,
purely used to 'prove' that the program is working:
A single line giving a variant position overview, i.e. gene name, chromosome,
reference base, etc.
A set of indented lines, with one of the above annotation values per line
(i.e. "\tdbsnp: rs1621733")
A blank line before the next variant
"""

# metadata
__author__ = "Bart Engels"
__status__ = "in development"
__version__ = '2019.d6.v1'

# Imports
import sys
import json
import argparse as arg
from operator import itemgetter

class ProcAno:
    """
    processed a annovar file
    """

    def __init__(self, file, g_str=False):
        """

        :param file: anovar file
        :param g_str: False
        """
        self.file = file
        self.g_str = g_str
        refseq_g_index = 16
        refseq_f_index = 15
        dbsnp_index = 27
        eur_index = 33
        sift_index = 34
        polyphen_index = 35
        clinvar_index = 53
        chro = 0
        pos = 6
        ref = 3
        obs = 4
        self.columns = [refseq_g_index, refseq_f_index, dbsnp_index, eur_index, sift_index,
                        polyphen_index, clinvar_index, chro, pos, ref, obs]

    def process_annovar_line(self, variant, header):
        """ Given a list of column indices, parse the header and all data lines """
        # Define the columns of interest
        columns = [1, 3, *range(8, 11), 15, *range(30, 36), 53]

        data = variant.split("\t")

        # Extract values for selected columns
        annotation_values = itemgetter(*self.columns)(data)

        # Return list of values if header is requested
        if not header:
            return annotation_values

        # Return all data as a dictionary with column: value
        return dict(
            zip(
                [field.strip() for field in header],
                [field.strip() for field in annotation_values]
            )
        )

    def parse_annovar(self):
        """
        parse annovar file  in to a list of dictionary's
        :return:  list with dictionary annovar data
        """
        header = []
        annovar_data = []

        with open(self.file) as data:
            for i, variant in enumerate(data):

                # Get header if we're processing the first line
                if i == 0:
                    header = self.process_annovar_line(variant, header=False)
                    continue  # proceed to next line

                # Process the data and add to a list
                annovar_data.append(self.process_annovar_line(variant, header))

        return annovar_data

    def __str__(self):
        """
        creats a string
        :return: string with anovar data
        """
        return str(json.dumps(self.parse_annovar(), indent=4))


def main(args):
    """
    main function
    """
    parser = arg.ArgumentParser()
    parser.add_argument('file', help='Annovar file, from galaxy server')
    argu = parser.parse_args()
    ano_file = argu.file
    print(ProcAno(ano_file))
    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv))
