#!/usr/bin/env python3
"""
This program makes a data base from a annovar file, by parsing the file and selecting
data that is needed, than putting it in a database.
by giving it a .my.cnf file you can easly use ure owne database
"""

__author__ = 'Bart Engels'
__status__ = 'program'
__version__ = '2019.d9.v1'


import sys
import argparse as asg

import mysql.connector
from mysql.connector import errorcode
from operator import itemgetter
import re


class DatabaseConnector:
    """
    this class parser the tabular file and put it in a database
    """

    def __init__(self, file, ano_file_data):
        """
        makes variables that are used for the connection with the database
        """
        self.my_db = mysql.connector.connect(
            option_files=file
        )
        self.ano = ano_file_data
        self.sql_aply = self.my_db.cursor()

    def conect_mysql(self):
        """
        connect with mysql and check if the param meters are falid.
        """
        try:
            self.my_db
        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
                print('Something is wrong with your user name or password')
            elif err.errno == errorcode.ER_BAD_DB_ERROR:
                print('Database does not exist')
            elif err.errno == errorcode.ER_ACCESS_DENIED_NO_PASSWORD_ERROR:
                print('No password!!')
            else:
                print(err)

    def fill_tables(self, data):
        """
        add information form the tabular file in to the database
        param: data is a dictionary that is created in ParsAnnovar
        """

        # sql syntax that executes the insert for table chromosomes
        add_chromosome = ('INSERT INTO chromosomes'
                          '(chromosome)'
                          'VALUES ({})')
        # sql syntax that executes the insert for table genes
        add_gene = ("INSERT INTO genes"
                    "(chr_id, RefSeq_Gene) "
                    "VALUES ({})")
        # sql syntax that executes the insert for table variants
        add_variant = ("INSERT INTO variants"
                       "(gene_id, POS, 1000g2015aug_EUR, dbsnp138, RefSeq_Func, LJB2_SIFT, LJB2_PolyPhen2_HDIV, LJB2_PolyPhen2_HVAR, CLINVAR,"
                       "reference, observed)"
                       "VALUES ({})")

        for d in data:
            query = f"select chr_id from chromosomes where chromosome = '{d['chromosome']}'"
            self.sql_aply.execute(query)
            if not self.sql_aply.fetchall():
                self.sql_aply.execute(add_chromosome.format("'{}'".format(d['chromosome'])))


            data_gene = (f"(select chr_id from chromosomes where chromosome = '{d['chromosome']}')",
                         "'{}'".format(d["RefSeq_Gene"]))
            # string format where the data is stored.
            data_variant = (f"""(select gene_id from genes where RefSeq_Gene= '{d['RefSeq_Gene']}')""",
                            "'{}'".format(d["POS"]), "'{}'".format(d["1000g2015aug_EUR"]),
                            "'{}'".format(d['dbsnp138']), "'{}'".format(d['RefSeq_Func']),
                            "'{}'".format(d["LJB2_SIFT"]), "'{}'".format(d["LJB2_PolyPhen2_HDIV"]), "'{}'".format(d["LJB2_PolyPhen2_HVAR"]),
                            "'{}'".format(d["CLINVAR"]), "'{}'".format(d["reference"]),
                            "'{}'".format(d["observed"]))

            query = f"select gene_id from genes where RefSeq_Gene = '{d['RefSeq_Gene']}'"

            self.sql_aply.execute(query)

            if not self.sql_aply.fetchall():
                self.sql_aply.execute(add_gene.format(", ".join(data_gene)))
            for value in d:
                if d[value] == '':
                    d[value] = "null"
            self.sql_aply.execute(add_variant.format(", ".join(data_variant)))
            self.my_db.commit()




    def summary(self):
        '''Prints summary of the newly added information'''
        # Chromosome table
        chromosomes = f"""select count(*) from chromosomes"""
        self.sql_aply.execute(chromosomes)
        print("The table chromosomes inside the database consists {} rows".format(self.sql_aply.fetchall()))

        # Gene table
        genes = f"select count(*) from genes"
        self.sql_aply.execute(genes)
        print("The table genes inside the database consists {} rows".format(self.sql_aply.fetchall()))

        # Variants table
        variants = f"select count(*) from variants"
        self.sql_aply.execute(variants)
        print("The table variants inside the database consists {} rows".format(self.sql_aply.fetchall()))


#################################################################################

class ParsAnnovar:
    """
    this class parses the annovar file, making use of the functions made in deliverable 6/7
    """
    def __init__(self, ano_file, g_str=False):
        self.file = ano_file
        self.g_str = g_str
        refseq_g_index = 16
        refseq_f_index = 15
        RefSeq_Func = 15
        dbsnp_index = 27
        eur_index = 33
        sift_index = 34
        polyphen_index = 35
        LJB2_PolyPhen2_HVAR = 36
        clinvar_index = 53
        chro = 0
        pos = 6
        ref = 3
        obs = 4
        self.colum = [refseq_g_index, refseq_f_index, chro, pos, eur_index, sift_index, dbsnp_index, RefSeq_Func,
                      polyphen_index, LJB2_PolyPhen2_HVAR, clinvar_index, ref, obs]


    def check_anovar(self):
        """
        Checks whether the file is in the correct format and motivates the user if it is.
        :return: if the annovar file is right format.
        """
        with open(self.file) as file:
            data = file.readline()
            i = 0
            for line in data:
                tap_line = line.split('\t')
                lineLength = len(tap_line)
                i = i + 1
                if lineLength < 1 and i < len(data):
                    outcome = "File is invalid format.***"
                    raise ValueError("File is invalid format.")
                else:
                    outcome = 'is the right file format!!***'
        return "***You did a great job this {}".format(outcome)


    def process_annovar_line(self, variant, header):
        """ Given a list of column indices, parse the header and all data lines
            :return: a dictionary with data from the header and annovar file.
        """
        # Define the columns of interest
        data = variant.split("\t")

        # Extract values for selected columns
        annotation_values = itemgetter(*self.colum)(data)

        # Return list of values if header is requested
        if not header:
            return annotation_values

        # Return all data as a dictionary with column: value
        return dict(
            zip(
                [field.strip() for field in header],
                [field.strip() for field in annotation_values]
            )
        )


    def parse_annovar(self):
        """
        parse the file and makes a list of data from the annovar file
        :return:  list with dictionary annovar data
        """
        header = []
        annovar_data = []

        with open(self.file) as data:
            for i, variant in enumerate(data):

                # Get header if we're processing the first line
                if i == 0:
                    header = self.process_annovar_line(variant, header=False)
                    continue  # proceed to next line

                # Process the data and add to a list
                annovar_data.append(self.process_annovar_line(variant, header))

        return annovar_data


    def clean_annovardata_dict(self):
        """
        cleans the annovardata dictonary from LOC and LINC an replaces it with a '-'
        """
        annovar_d = []
        for dict in self.parse_annovar():
            Refseq = dict['RefSeq_Gene']
            # regular expression to filter the collum gene form the file
            reg_ex = r'(\w+)(\([\w\:\.\+\ > \, \=]+\)|\d +])?\,?(\w+)?(\([\w\:\.\+\ > \, \=]+\) |\d +])?'
            gene = re.match(reg_ex, Refseq)
            if gene[1] in 'NONE':
                gene = '-'
            elif gene[0].startswith('LOC') or gene[0].startswith('LINC'):
                gene = '-'
            elif gene[3] in [None, 'NONE']:
                gene = gene.group(1)
            else:
                gene = gene.group(1) + '/' + gene.group(3)
            for key in dict.keys():
                if key == 'RefSeq_Gene':
                    dict[key] = gene
            annovar_d.append(dict)
        return annovar_d


def main(args):
    """
    makes sure everything does what is suppose to
    """
    parser = asg.ArgumentParser()

    parser.add_argument('file', help='Give .my.cnf')

    parser.add_argument('annovar_file', help='Annovar annotation file')

    args = parser.parse_args()
    file = args.file
    ano_file = args.annovar_file

    parsed_ano = ParsAnnovar(ano_file)
    print(parsed_ano.check_anovar())
    print('#' * 100)
    parsed_ano.parse_annovar()
    parsed_ano.clean_annovardata_dict()

    a_mysql = DatabaseConnector(file,  parsed_ano)
    a_mysql.conect_mysql()
    a_mysql.fill_tables(parsed_ano.clean_annovardata_dict())
    a_mysql.summary()



    return 0


if __name__ == "__main__":
    sys.exit(main(sys.argv))
