#!/usr/bin/env python3

"""
BFV2 Theme 05 - Genomics - Sequencing Project

Template for parsing and filtering VCF data given a certain variant
allele frequency value.

Deliverable 5
-------------
Make changes to the `parse_vcf_data` function AND the `main` function,
following the instructions preceded with double '##' symbols.

    usage:
        python3 deliverable5.py vcf_file.vcf frequency out_file.vcf

    arguments:
        vcf_file.vcf: the input VCF file, output from the varscan tool
                      frequency: a number (integer) to use as filtering value
        out_file.vcf: name of the output VCF file

    output:
        a VCF file containing the complete header (comment lines) and
        only the remaining variant positions after filtering.
"""

# METADATA VARIABLES
__author__ = "Bart Engels"
__status__ = "program"
__version__ = "2019.d5.v1"

# IMPORT
import sys
import argparse

def parse_vcf_data(vcf_input_file, frequency, vcf_output_file):
    """ This function reads the input VCF file line by line, skipping the first
    n-header lines. The remaining lines are parsed to filter out variant allele
    frequencies > frequency.
    """

    ## Open the INTPUT VCF file, read the contents line-by-line
    with open(vcf_input_file) as vcf_file:
        new_file = open(vcf_output_file, 'w+')
        ## Iterate over every line in vcf_file:
        for line in vcf_file:
            vcf_line = line
            ## Writes needed data from vcf_file to new_file
            if vcf_line.startswith('#'):
                new_file.write(vcf_line)
            elif vcf_line.startswith('chr'):
                vcf_line = vcf_line.split()
                vcf_freq_line = vcf_line[9].split(':')
                freq = float(vcf_freq_line[6].replace('%', ''))
                if freq > frequency:
                    new_file.write("{}".format(line))

# MAIN
def main(args):
    """ Main function """

    ### INPUT ###
    my_parser = argparse.ArgumentParser()

    my_parser.add_argument('input', help='your input vcf file')
    my_parser.add_argument('output', help='your output vcf file')
    my_parser.add_argument('freq', help='the frequency of the variant allele', type=float)

    args = my_parser.parse_args()

    vcf_file = args.input
    out_vcf = args.output
    frequency = args.freq

    # Process the VCF-file
    parse_vcf_data(vcf_file, frequency, out_vcf)

    return 0

if __name__ == "__main__":
    sys.exit(main(sys.argv))
